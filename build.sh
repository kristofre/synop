#!/bin/env bash

function create {
    docker build -t registry.gitlab.com/checkelmann/synop .
    docker push registry.gitlab.com/checkelmann/synop

    kubectl apply -f synop.yaml
    sleep 15
    kubectl logs -f $(kubectl get pods | grep synop | awk {'print $1'})
}

function delete {    
    kubectl delete -f synop.yaml
}

if [ "$1" == "create" ]; then
    create
fi
if [ "$1" == "delete" ]; then
    delete
fi
if [ "$1" == "recreate" ]; then
    delete
    create
fi
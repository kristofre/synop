import kopf
import json
import kubernetes
import time
import os
import requests
from requests.exceptions import HTTPError

def delete_synthetic(name, environment, logger):
    tenant = os.getenv('DT_TENANT', 'none')
    api_token = os.getenv('DT_API_TOKEN', 'none')
    header = {"Authorization":"Api-Token " + api_token}

    url = "https://" + tenant + ".live.dynatrace.com/api/v1/synthetic/monitors?tag=app:" + name + "&tag=environment:" + environment
    r=json.loads(requests.get(url, headers=header).text)
    header = {
        "Authorization":"Api-Token " + api_token,
        "Content-type": "application/json"
        }
    
    if (len(r["monitors"]) > 0):
        logger.info("Found monitor with id " + r["monitors"][0]["entityId"])
        logger.info(r["monitors"][0]["entityId"])
        url = "https://" + tenant + ".live.dynatrace.com/api/v1/synthetic/monitors/" + r["monitors"][0]["entityId"]
        try:
            r = requests.delete(url,headers=header)
            r.raise_for_status()
        except HTTPError as http_err:
            logger.error(r.text)
        except Exception as err:
            logger.error(r.text)
        else:
            logger.info(r.text)        
    else:
        logger.info("No synthetic monitor found for " + name + ".")
        


def create_sythetic(endpoint: str, name: str, environment: str, tags: list, locations: list, manuallyAssignedApps: list, logger, frequencyMin: int = 15):
    tenant = os.getenv('DT_TENANT', 'none')
    api_token = os.getenv('DT_API_TOKEN', 'none')
    header = {"Authorization":"Api-Token " + api_token}

    default_tags = [
        'environment:' + environment,
        'app:' + name,
    ]

    payload_tags = tags + default_tags

    # If Synthetic Locations are provides, use them
    if (len(locations) > 0 ):
        t_loc = locations
    else:
        url = "https://" + tenant + ".live.dynatrace.com/api/v1/synthetic/locations?type=PRIVATE"
        try:
            r=requests.get(url, headers=header)
            r.raise_for_status()
        except HTTPError as http_err:
            logger.error(r.text)
        except Exception as err:
            logger.error(r.text)
        else:
            logger.info(r.text)
        locations=json.loads(r.text)
        t_loc=""
        for i in range(len(locations["locations"])):
            t_ent = locations["locations"][i]["entityId"]
            if (i < len(locations["locations"])):        
                t_loc = t_loc + t_ent
            else:
                t_loc = t_loc + t_ent + ','

    payload = {
    'frequencyMin': frequencyMin,
    'anomalyDetection': {
    'outageHandling': {
    'globalOutage': True,
    'localOutage': False,
    'localOutagePolicy': {
        'affectedLocations': 1,
        'consecutiveRuns': 3
    }
    },
    'loadingTimeThresholds': {
        'enabled': False,
        'thresholds': [
            {
            'type': 'TOTAL',
            'valueMs': 10000
            }
        ]
    }
    },
    'type': 'HTTP',
    'name': name + " - " + environment + " - Ping",
    'locations': [
        t_loc
    ],
    'enabled': True,
    'script': {
    'version': "1.0",
    'requests': [
        {
            'description': name,
            'url': endpoint,
            'method': 'GET',
            'requestBody': '',
            'configuration': {
            'acceptAnyCertificate': True,
            'followRedirects': True
            },
            'preProcessingScript': '',
            'postProcessingScript': ''
        }
    ]
    },
    'tags': payload_tags,
    'manuallyAssignedApps': manuallyAssignedApps
    }

    # Check if synthetic exists
    # entityId=$(curl -s -X GET "${API}/monitors?tag=app:${APPLICATION_SHORT_NAME}&tag=environment:${CI_ENVIRONMENT_SLUG}" -H "Authorization: Api-Token ${DT_API_TOKEN}" | jq .monitors[0].entityId)
    url = "https://" + tenant + ".live.dynatrace.com/api/v1/synthetic/monitors?tag=app:" + name + "&tag=environment:" + environment
    r=json.loads(requests.get(url, headers=header).text)
    header = {
        "Authorization":"Api-Token " + api_token,
        "Content-type": "application/json"
        }
    if (len(r["monitors"]) > 0):
        logger.info("Found existing")
        logger.info(r["monitors"][0]["entityId"])
        url = "https://" + tenant + ".live.dynatrace.com/api/v1/synthetic/monitors/" + r["monitors"][0]["entityId"]
        r = requests.put(url,json=payload,headers=header)
        logger.info(r.text)
    else:
        logger.info("Create new Synthetic")
        url = "https://" + tenant + ".live.dynatrace.com/api/v1/synthetic/monitors"
        
        try:
            r = requests.post(url,json=payload,headers=header)
            r.raise_for_status()
        except HTTPError as http_err:
            logger.error(r.text)
        except Exception as err:
            logger.error(r.text)
        else:
            logger.info(r.text)

    return

@kopf.on.delete('extensions', 'v1beta1', 'ingresses', annotations={'synop/create': 'true'})
def ingress_delete(spec, meta, status, logger, **kwargs):
    logger.info("Ingress with Synthetic will be deleted...")
    annotations=meta.get('annotations')
    logger.info(meta["name"])
    logger.info(meta["namespace"])
    delete_synthetic(meta["name"], meta["namespace"], logger)

@kopf.on.create('extensions', 'v1beta1', 'ingresses', annotations={'synop/create': 'true'})
def ingress_create(spec, meta, status, logger, **kwargs):      
    annotations=meta.get('annotations')
    logger.info(meta["name"])
    logger.info(meta["namespace"])
    logger.info(spec)
    logger.info(meta)
    logger.info(status)
        
    # Check if host is configured
    try:
        hostname = spec['rules'][0]['host']
        logger.info("Found hostname in spec: " + hostname)
        skip_ingress = False
    except KeyError:
        logger.info("No hostname in spec, try to find it...")
        skip_ingress=True
        # Try to find Hostename from Ingress        
        if (annotations["kubernetes.io/ingress.class"].upper() == "ALB"):
            api = kubernetes.client.ExtensionsV1beta1Api()
            for i in range(0,10):
                logger.info("Waiting for ingress hostname...")                
                try:
                    ret = api.read_namespaced_ingress_status(meta["name"],meta["namespace"])
                    hostname=ret.status.load_balancer.ingress[0].hostname
                    logger.info("Hostname found: " + ret.status.load_balancer.ingress[0].hostname)
                    skip_ingress=False
                    break
                except:
                    logger.info("Hostname not found.")
                
                time.sleep(3)

    # Check if Tags are provided
    try:
        tags = annotations["synop/synthetic.tags"].split(",")
    except KeyError:
        tags = []

    try:
        locations = annotations["synop/synthetic.locations"].split(",")
    except KeyError:
        locations = []

    try:
        env = annotations["synop/environment"]
    except KeyError:
        env = meta["namespace"]

    try:
        frequencyMin = int(annotations["synop/synthetic.frequencyMin"])
    except KeyError:
        frequencyMin = 15


    try:
        manuallyAssignedApps = annotations["synop/synthetic.manuallyAssignedApps"].split(",")
    except KeyError:
        manuallyAssignedApps = []

    try:
        appname = annotations["synop/name"]
    except KeyError:
        appname = meta["name"]

    # Set Protocol which should be used
    try:
        is_tls = spec['tls']
        uri = "https://"
    except KeyError:
        uri = "http://"             

    try:
        uri = annotations["synop/synthetic.protocol"]
        logger.info("Detected protocol will be overwritten with " + uri)
    except KeyError:
        logger.info("Detected protocol " + uri + " will be used.")


    if(skip_ingress):
        logger.info("Skipping Ingress.")
    else:
        try:
            endpoint = annotations["synop/endpoint"]
        except KeyError:
            endpoint = ""
        
        url = uri + hostname + endpoint
        logger.info("Create synthetic for URL " + url)
        
        create_sythetic(endpoint=url, name=appname, environment=env, 
            tags=tags, frequencyMin=frequencyMin, 
            locations=locations, manuallyAssignedApps=manuallyAssignedApps, logger=logger)

# Thats it!
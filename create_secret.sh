#!/bin/env bash

tenant=$1
apikey=$2

tenant=$(echo -n ${tenant} | base64)
apikey=$(echo -n ${apikey} | base64)

cat << EOF > secret_synop.yaml
---
apiVersion: v1
kind: Secret
metadata:
    name: synop-credentials
type: Opaque
data:
    DT_TENANT: ${tenant}
    DT_API_TOKEN: ${apikey}
EOF